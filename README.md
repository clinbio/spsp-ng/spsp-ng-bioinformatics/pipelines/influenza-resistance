# Influenza resistance

Tool for calling resistance mutations from Influenza sequences.
Currently, the tool comes with reference mutations for PA and NA as listed by the World Health Organization (WHO).

The folder "reference_datasets" contains:
- WHO mutations lists (converted from PDF to TSV files):
  - Human PA: https://www.who.int/publications/m/item/summary-of-polymerase-acidic-(pa)-protein-amino-acid-substitutions-analysed-for-their-effects-on-baloxavir-susceptibility 
  - Human NA:  https://www.who.int/publications/m/item/summary-of-neuraminidase-(na)-amino-acid-substitutions-associated-with-reduced-inhibition-by-neuraminidase-inhibitors-(nais) 
- A description file which assigns a reference protein FASTA and mutations file for each type/subtype/lineage/protein. The corresponding FASTA and mutations files are expected in the "reference_datasets" folder. The description file may be extended to other Influenza types and proteins.

## Requirements

- Docker

## Running the script

The script requires at least the following information:
- FASTA filename (this can be a multi-FASTA containing several segments of the same isolate)
- Gene ("PA" or "NA")
- Host (currently only "human")
- Type ("A", "B", "C", "D")

Optional:
- Subtype ("HxNx")
- Lineage ("seasonal", "variant", "yamagata", "victoria")

Get Help:
```
./influenza_resistance_annotation.py -h

  -h, --help            show this help message and exit
  --fasta_file FASTA_FILE
                        REQUIRED. FASTA filename
  --protein {PB2,PB1,PA,HA,NP,NA,M1,M2,NS1,NS2}
                        REQUIRED. Influenza segment of interest. Note: Provided Docker reference dataset only supports NA and PA.
  --host {Human,human,Avian,avian,Swine,swine}
                        REQUIRED. Host. Provided Docker reference dataset only supports Human.
  --type {A,B,C,D}      REQUIRED. Influenza type.
  --subtype SUBTYPE     Optional. Influenza A subtype: HxNy, where x,y are numbers, e.g. H3N2. Default is empty string.
  --lineage LINEAGE     Optional. Influenza lineage: pdm09, seasonal, variant (for H3N2 of swine origin). Default is empty string.
  --output OUTPUT       Optional. Output filename. Default is results.json.
  --version             show program's version number and exit
```

After pulling the code from the repository, build the Docker image and run the script from the image.
```
cd influenza_resistance
docker build -t influenzaresistance:latest .
docker run -it influenzaresistance:latest
```

Run the script on test data:
```
./influenza_resistance_annotation.py --fasta_file test_data/A_NA_N1_mutated.fasta --protein NA --host human --type A --subtype H1N1 --lineage pdm09
```
```
./influenza_resistance_annotation.py --fasta_file test_data/A_NA_N2_deletion.fasta --protein NA --host human --type A --subtype H3N2 --lineage seasonal
```

The output is written as a JSON file (the default filename "results.json" can be changed using the --output option). Example output below:
```
{
    "input": {
        "fasta_file": "test_data/A_NA_N1_mutated.fasta",
        "protein": "NA",
        "host": "human",
        "type": "A",
        "subtype": "H1N1",
        "lineage": "pdm09",
        "output": "results.json",
        "version": "1.0.2",
        "reference_genome":"OR533783.1"
    },
    "output": {
        "mutations": [
            "Q313K+I427T"
        ]
    }
 }   
```

## License
The source code is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.

## Project status
Currently implemented for human Influenza for proteins PA and NA. Further work required to extend to other hosts and proteins. Contributions welcome.
