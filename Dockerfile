FROM condaforge/miniforge3:latest
COPY . /app
RUN mamba env update -n base -f /app/environment.yml
WORKDIR /app