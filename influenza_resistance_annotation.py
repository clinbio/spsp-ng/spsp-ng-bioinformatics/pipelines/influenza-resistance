#!/usr/bin/env python
# coding: utf-8

tool_version = '1.0.6'

import platform
import argparse
import pandas as pd
import os
import sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import subprocess
import itertools
import json
from itertools import groupby

ref_datasets_path = "reference_datasets"
description_file = "description_file.txt"

# Define list of Influenza proteins
influenza_proteins = ["PB2", "PB1", "PA", "HA", "NP", "NA", "M1", "M2", "NS1", "NS2"]

# Define start codon
start_codon = "ATG"

# Define a dictionary mapping ambiguous amino acid codes to their possible amino acids
iupac_nucleic_acid_mapping = {
    'A': ['A'],
    'T': ['T'],
    'G': ['G'],
    'C': ['C'],
    'U': ['U'],
    'R': ['A', 'G'],  # Purine (A or G)
    'Y': ['C', 'T'],  # Pyrimidine (C or T)
    'S': ['G', 'C'],
    'W': ['A', 'T'],
    'K': ['G', 'T'],
    'M': ['A', 'C'],
    'B': ['C', 'G', 'T'],  # Not A
    'D': ['A', 'G', 'T'],  # Not C
    'H': ['A', 'C', 'T'],  # Not G
    'V': ['A', 'C', 'G'],  # Not T
    'N': ['A', 'C', 'G', 'T'],  # Any
}


def set_bin_path():
    system = platform.system()
    architecture = platform.machine()

    if system == "Windows":
        return os.path.join('bin', "muscle5.1.win64.exe")
    elif system == "Linux":
        return os.path.join('bin', "muscle5.1.linux_intel64")
    elif system == "Darwin":
        if architecture == 'arm64':
            return os.path.join('bin', "muscle5.1.macos_arm64")
        elif architecture == 'x86_64':
            return os.path.join('bin', "muscle5.1.macos_intel64")
        else:
            raise Exception("Error: operating system not supported.")
    else:
        raise Exception("Error: operating system not supported.")


def load_file(filename):
    """
    Load TSV file
    :param filename:str. File path/name to load
    :return: pandas.DataFrame containing the information in the file.
    """

    try:
        if 'xls' in filename.split('.')[-1]:
            df = pd.read_excel(filename, engine='openpyxl', header=3)
        elif ('tsv' in filename.split('.')[-1]) | ('txt' in filename.split('.')[-1]):
            df = pd.read_csv(filename, sep='\t', comment='#', header=0, engine='python',
                             na_filter=False)  # na_filter required because of the NA protein name
        else:
            raise Exception("The metadata file extension should be xls, xlsx, tsv or txt.")
    except:
        raise Exception("Error when loading file: {}.".format(filename))
    return df


def count_sequences(fasta_file):
    # Initialize a counter for sequences
    num_sequences = 0

    # Open the FASTA file and read line by line
    with open(fasta_file, 'r') as f:
        for line in f:
            # Check if the line starts with '>'
            if line.startswith('>'):
                # Increment the sequence counter
                num_sequences += 1

    return num_sequences


def translate(record, type, protein):
    # Find start codon
    start_codon_index = record.seq.upper().find(start_codon)
    if start_codon_index > 0:
        if (type == "B") & (
                protein == "NA"):  # in Influenza B, the NA nucleotide sequence encodes NB and NA. If a second start codon is found in the first 42 positions, it suggests that the NA CDS starts there rather than at the first start codon (initiating the NB CDS).
            start_codon_index2 = record.seq[start_codon_index + 3:].upper().find(
                start_codon)  # look for a 2nd start codon
            if (start_codon_index2 > 0) & (
                    start_codon_index2 < 42):  # if 2nd start codon is in the first 42 positions, start coding sequence for NA at that position
                start_codon_index = start_codon_index + 3 + start_codon_index2
    elif start_codon_index < 0:
        raise Exception(f"Error: No start codon {start_codon} was found in the provided sequence.")
    # print(f"Start codon found at position {start_codon_index}")

    cds = record.seq[start_codon_index:]
    cds = Seq(str(cds).replace("-",
                               ""))  # remove any gaps in the input sequence since we will translate and re-align with our reference
    while len(cds) % 3 != 0:  # trim to have only multiples of 3
        cds = cds[:-1]
    protein_seq = cds.translate(to_stop=True)
    protein_seqs, nb_unambiguous_prot_seq = correct_protein_seq_ambiguousAA(protein_seq, cds)
    # print(f"\n>Translated sequence for {record.id}")
    # print(protein_seqs)
    return protein_seqs, nb_unambiguous_prot_seq


def replace_char_at_index(s, index, replacement):
    if 0 <= index < len(s):
        # Construct a new string with the character replaced
        return s[:index] + replacement + s[index + 1:]
    else:
        # Index out of bounds, return original string or handle error as needed
        return s


def correct_protein_seq_ambiguousAA(protein_seq, dna_seq):
    # correct positions where ambiguous codon was translated as X. This will generate a list of protein sequences from where to look for mutations.
    if "X" in protein_seq:
        posX = [pos for pos, c in enumerate(protein_seq) if c == "X"]
        corrected_protein_seq = []
        for x in posX:
            ambiguous_codon = str(dna_seq)[x * 3:x * 3 + 3]
            # Generate all possible unambiguous codons
            possible_codons = list(itertools.product(*[iupac_nucleic_acid_mapping[nuc] for nuc in ambiguous_codon]))
            # Translate each codon and collect possible amino acids
            possible_amino_acids = {Seq(''.join(codon)).translate() for codon in possible_codons}
            for aa in possible_amino_acids:
                corrected_protein_seq.append(replace_char_at_index(protein_seq, x, aa))
        n = len(corrected_protein_seq)
    else:
        corrected_protein_seq = [protein_seq]
        n = 1

    return corrected_protein_seq, n


def count_consecutive_characters_and_positions(s, target_char):
    # Initialize the current position
    current_position = 0
    # List to store the length and start position of each run
    runs = []

    # Use groupby to group consecutive characters
    for char, group in groupby(s):
        # Calculate the length of the current group
        length = sum(1 for _ in group)
        # If the current character is the target, save the length and start position
        if char == target_char:
            runs.append((length, current_position))
        # Update the current position
        current_position += length

    return runs


def correct_alignement_to_the_right(ref_seq, target_seq):
    '''
    Example to correct:
    REF: GNATG
    ALT: ----G
    Should become
    REF: GNATG
    ALT: G----
    This reflects the biology of what a polymerase would read
    '''

    realigned_target_seq = str(target_seq)

    # Find deletion # for now, only works with 1 deletion per sequence
    runs_and_positions = count_consecutive_characters_and_positions(target_seq, '-')
    first_deletion_pos = runs_and_positions[0][1]
    end_deletion_pos = runs_and_positions[0][1] + runs_and_positions[0][0]
    aa_ref = ref_seq[first_deletion_pos]
    aa_end = target_seq[end_deletion_pos]  # aa at the end of the deletion

    if aa_ref == aa_end:
        old_del = str(target_seq[first_deletion_pos:end_deletion_pos + 1])
        new_del = aa_ref + str(target_seq[first_deletion_pos:end_deletion_pos])
        realigned_target_seq = realigned_target_seq.replace(old_del, new_del)

    return realigned_target_seq


def align_pairwise(ref_seq, target_seq, path_to_alignment_tool):
    input_fasta = os.path.join('/tmp', 'input_fasta_tmp.fasta')
    output_fasta = os.path.join('/tmp', 'output_fasta_tmp.fasta')
    cmd = (path_to_alignment_tool, "-align", input_fasta, "-output", output_fasta)

    seq_ref_record = SeqRecord(ref_seq, id="ref_seq", description="ref_seq")
    seq_target_record = SeqRecord(target_seq, id="target_seq", description="target_seq")

    # Writing multi-FASTA input file
    with open(input_fasta, 'w') as output_handle:
        SeqIO.write([seq_ref_record, seq_target_record], output_handle, 'fasta')
    popen = subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, shell=False)
    popen.wait()

    alignment = []
    for record in SeqIO.parse(output_fasta, 'fasta'):
        alignment.append(record.seq)

    if len(alignment) < 2:
        raise Exception("Error: less than 2 sequences found in the aligner output FASTA file.")

    if '-' in alignment[1]:
        corrected_sequence = correct_alignement_to_the_right(alignment[0], alignment[1])
        alignment.pop()
        alignment.append(corrected_sequence)

    os.remove(input_fasta)
    os.remove(output_fasta)
    return alignment


def check_presence_mutations(reference_seq, protein_seq, mutations):
    mutations_present = []
    for mut in mutations:
        if "+" in mut:
            muts = mut.split("+")  # get all individual mutations
            mutation_present = 0
            for mutis in muts:
                ref_aa = mutis[0]
                pos = mutis[1:-1]
                alt_aa = mutis[-1]
                if (protein_seq[int(pos) - 1] == alt_aa) & (reference_seq[int(pos) - 1] == ref_aa):
                    mutation_present += 1

            if mutation_present == len(muts):  # meaning that all individual mutations are present
                mutations_present.append(mut)

        elif "Del" in mut:
            muts = mut.split(" ")[1].split("-")
            pos_start = int(muts[0])
            pos_end = int(muts[1])
            if protein_seq[pos_start - 1:pos_end] == "-" * (pos_end - pos_start + 1):
                mutations_present.append(mut)

        else:  # single nucleotide mutation
            ref_aa = mut[0]
            pos = mut[1:-1]
            alt_aa = mut[-1]

            if (protein_seq[int(pos) - 1] == alt_aa) & (reference_seq[int(pos) - 1] == ref_aa):
                mutations_present.append(mut)

    # if a mutation is part of a combination, remove the individual call, e.g. E119V, E119V+T148I should be only E119V+T148I
    for mut in mutations_present:
        if any(mut in item and mut != item for item in mutations_present):
            mutations_present.remove(mut)

    return mutations_present


def main():
    ## Arguments
    parser = argparse.ArgumentParser(
        description='CLI to retrieve resistance mutations from Influenza sequences. The provided parameters should match the values provided in the reference_datasets/description_file.txt.')
    parser.add_argument('--fasta_file', type=str, required=True, help='REQUIRED. FASTA filename')
    parser.add_argument('--protein', type=str, required=True,
                        choices=["PB2", "PB1", "PA", "HA", "NP", "NA", "M1", "M2", "NS1", "NS2"],
                        help='REQUIRED. Influenza segment of interest. Note: Provided Docker reference dataset only supports NA and PA.')
    parser.add_argument('--host', type=str, required=True,
                        choices=["Human", "human", "Avian", "avian", "Swine", "swine"],
                        help='REQUIRED. Host. Provided Docker reference dataset only supports Human.')
    parser.add_argument('--type', type=str, required=True, choices=["A", "B", "C", "D"],
                        help='REQUIRED. Influenza type.')
    parser.add_argument('--subtype', type=str, default="",
                        help='Optional. Influenza A subtype: HxNy, where x,y are numbers, e.g. H3N2. Default is empty string.')
    parser.add_argument('--lineage', type=str, default="",
                        help='Optional. Influenza lineage: pdm09, seasonal, variant (for H3N2 of swine origin). Default is empty string.')
    parser.add_argument('--output', type=str, default="results.json",
                        help='Optional. Output filename. Default is results.json.')
    parser.add_argument('--version', action='version', version=f'%(prog)s {tool_version}')
    args = parser.parse_args()

    # Binary path to alignment tool
    path_to_alignment_tool = set_bin_path()

    # Check if FASTA comes with a single record or as a multi-FASTA with several sequences for each segment of the isolate
    nb_records_in_FASTA = count_sequences(args.fasta_file)
    if nb_records_in_FASTA < 1:
        raise Exception("Error: at least one sequence is expected in the FASTA file")
    elif nb_records_in_FASTA == 1:
        multi_fasta = 0
    else:
        multi_fasta = 1

    # Generate translation of FASTA file (nucleic acids to amino acids). ! Assumes only 1 sequence in the FASTA file
    for record in SeqIO.parse(args.fasta_file, "fasta"):
        # if multi-FASTA,
        if multi_fasta:
            if record.id[-2:] == args.protein:  ## assumes that the protein is indicated at the end of the record id
                # Translate the sequence
                protein_seqs, nb_unambiguous_prot_seq = translate(record, args.type, args.protein)
                break
        else:
            # Translate the sequence
            protein_seqs, nb_unambiguous_prot_seq = translate(record, args.type, args.protein)
    if nb_unambiguous_prot_seq < 1:
        raise Exception(
            "Error: no nucleotide sequence for the segment of interest could be found in the provided FASTA file.")

    # Load reference protein sequence for alignment
    df = load_file(os.path.join(ref_datasets_path, description_file))
    reference_FASTA = df[
        (df['Host'] == args.host.lower()) & (df['Type'] == args.type) & (df['Subtype'] == args.subtype) & (
                    df['Lineage'] == args.lineage) & (df['Protein'] == args.protein)]["Reference sequence filename"]
    if len(reference_FASTA) < 1:
        raise Exception("Error: no matching reference protein sequence was found.")
    with open(os.path.join(ref_datasets_path, reference_FASTA.values[0]), "r") as file:
        for record in SeqIO.parse(file, "fasta"):
            reference_seq = record.seq

    reference_genome_accession = df[
    (df['Host'] == args.host.lower()) & (df['Type'] == args.type) & (df['Subtype'] == args.subtype) & (
                df['Lineage'] == args.lineage) & (df['Protein'] == args.protein)]["Reference sequence identifier"]
    if len(reference_genome_accession) < 1:
        raise Exception("Error: no matching reference protein sequence identifier was found.")

    # Load resistance files for the host, type, subtype, lineage and protein
    resistance_file = df[
        (df['Host'] == args.host.lower()) & (df['Type'] == args.type) & (df['Subtype'] == args.subtype) & (
                    df['Lineage'] == args.lineage) & (df['Protein'] == args.protein)]["Resistance filename"]
    df = load_file(os.path.join(ref_datasets_path, resistance_file.values[0]))

    # Select mutations of interest given the type, subtype, lineage
    if args.lineage in ["yamagata", "Yamagata", "victoria",
                        "Victoria"]:  # the WHO resistance table does not differentiate between these lineages for B
        args.lineage = " "
    filtered_df = df[(df['Type'] == args.type) & (df['Subtype'] == args.subtype) & (df['Lineage'] == args.lineage)]
    mutations = filtered_df['AA']
    # print(f"\n>Mutations of interest for {args.protein}:")
    # print(filtered_df['AA'])

    all_mutations_present = []
    for protein_seq in protein_seqs:
        # align sequences
        alignment = align_pairwise(reference_seq, protein_seq, path_to_alignment_tool)
        reference_seq = alignment[0]
        protein_seq_aligned = alignment[1]

        # print("\n>Aligned ref sequence:")
        # print(reference_seq)

        # print("\n>Aligned sequence:")
        # print(protein_seq_aligned)

        # Test presence of resistance mutations in the translated sequence
        # print(f"\n>Identified mutations in {args.fasta_file} {args.protein}:")
        mutations_present = check_presence_mutations(reference_seq, protein_seq_aligned, mutations)
        all_mutations_present.append(mutations_present)

    all_mutations_present = [item for sublist in all_mutations_present for item in sublist]  # flatten list
    all_mutations_present = list(set(all_mutations_present))  # keep unique elements
    # print(all_mutations_present)

    content = {
        "input": {
            "fasta_file": args.fasta_file,
            "protein": args.protein,
            "host": args.host,
            "type": args.type,
            "subtype": args.subtype,
            "lineage": args.lineage,
            "output": args.output,
            "version": tool_version,
            "reference_genome": reference_genome_accession.values[0]
        },
        "output": {
            "mutations": all_mutations_present
        }
    }

    pretty_content = json.dumps(content, indent=4)

    with open(args.output, 'w') as f:
        f.write(pretty_content)
    f.close()


if __name__ == "__main__":
    main()
